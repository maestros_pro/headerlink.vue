import MFormWrap     from './form.vue';
import MFormSelect   from './select.vue';
import MFormInput    from './input.vue';
import MFormTextarea from './textarea.vue';
import MFormCheckbox from './checkbox.vue';
import MFormRadio    from './radio.vue';
import MFormButton   from './button.vue';

export {
	MFormWrap,
	MFormButton,
	MFormSelect,
	MFormInput,
	MFormTextarea,
	MFormCheckbox,
	MFormRadio,
};


/**
 *
 * 	rules: {
 * 		empty: 'Сообщение ошибки',
		pattern: [
			{
				regex: /^(http(s)?:\/\/)?(www\.)?vk\.com\/[A-z0-9_\-.]+\/?/i,
				message: 'Проверьте корректность ссылки'
			}
		],
	}
 *
 */