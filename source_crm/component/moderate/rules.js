import {rules as Api} from '@api'
import {clearObject} from '@util'

export default {
	namespaced: true,

	state: {
		list: [],

		related: [],

		recently: [],

		item: {
			breadcrumbs: [],
			id: null,
			name: '',
			text: '',
		},

		filter: {
			rulesType: null,
			search: '',
			searchInText: false,
		}
	},

	mutations: {
		SET_LIST(state, value) {
			state.recently = value.recently ? [...value.recently] : [];
			state.related = value.related ? [...value.related] : [];
			state.list = value.rules ? [...value.rules] : [];
		},

		SET_ITEM(state, data) {
			let obj = {}
			if ( data ){
				obj.breadcrumbs = data.breadcrumbs || [];
				obj.id = data.id || null;
				obj.name = data.name || '';
				obj.text = data.text || '';
			}
			state.item = JSON.parse(JSON.stringify(Object.assign({}, obj)));
		},

		SET_FILTER_PROP(state,{name, value}) {
			state.filter[name] = value;
		},

	},

	actions: {

		async getList({commit, state}){
			const value = await Api.getList(clearObject(state.filter));
			commit("SET_LIST", value);
			return value;
		},

		async getItem({commit, state}, id){
			commit("SET_ITEM");
			const value = await Api.getItem(id);
			commit("SET_ITEM", value.item);
			return value;

		},

	},

}