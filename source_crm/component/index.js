import Corpus from './_corpus'
import Popup from './_popup'
import FormField from './_form'
import FormWrap from './_form-wrap'
import Icon from './_icon'
import TableWrap from './_table-wrap'
import KitWrap from './_kit-wrap'
import BtnItem from './_btn-item'
import BtnGroup from './_btn-group'
import FakeText from './_fake-text'
import ImagesPreview from './_images-preview'
import SlideUpDown from 'vue-slide-up-down'
import CheckLink from './check-link'
import PassProgress from './_pass-progress'
import HeaderWrap from './layout/header'

import {
	MFormWrap,
	MFormSelect,
	MFormInput,
	MFormTextarea,
	MFormCheckbox,
	MFormRadio,
	MFormButton,
} from './m-form'

export default {
	Popup,
	Icon,
	Corpus,
	CheckLink,
	SlideUpDown,
	FormField,
	FormWrap,
	TableWrap,
	KitWrap,
	BtnItem,
	BtnGroup,
	FakeText,
	PassProgress,
	ImagesPreview,
	MFormWrap,
	MFormInput,
	MFormTextarea,
	MFormCheckbox,
	MFormSelect,
	MFormRadio,
	MFormButton,
	HeaderWrap
}