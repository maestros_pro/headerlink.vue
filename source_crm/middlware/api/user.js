import axios from './instance'

export default {

	getData(){
		return axios.post('', {action: 'getCurrentUserInfo'}).then(r => r.data)
	},

	saveCompany(data){
		return axios.post('', {action: 'addCompany', ...data})
	},

	updateUserImage(data){
		return axios.post('', {action: 'updateUserImage', ...data})
	},


}