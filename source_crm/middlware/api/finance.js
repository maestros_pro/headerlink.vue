import axios from './instance'

export default {

	getFinanceKit(data){
		return axios.post('', {action: 'getFinanceKit', ...data}).then(r => r.data)
	},

	getFinanceCources(){
		return axios.post('', {action: 'getFinanceCources'}).then(r => r.data)
	},

	getFinanceSettings(){
		return axios.post('', {action: 'getFinanceSettings'}).then(r => r.data)
	},

	saveFinanceSettings(data){
		return axios.post('', {action: 'saveFinanceSettings', ...data}).then(r => r.data)
	},


	getFinanceItemChart(id){
		return axios.post('', {action: 'getFinanceItemChart', id}).then(r => r.data)
	},


}