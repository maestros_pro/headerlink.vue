import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getShowcaseList', ...data}).then(r => r.data)
	},

	saveItem(data){
		return axios.post('', {action: 'insertShowcase', ...data}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'loadShowcase', id}).then(r => r.data)
	},

	deleteItem(id){
		return axios.post('', {action: 'deleteShowcase', delete: id}).then(r => r.data)
	},

}