import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getModerateRulesList', ...data}).then(r => r.data)
	},

	getItem(data){
		return axios.post('', {action: 'getModerateRuleItem', ...data}).then(r => r.data)
	},

}