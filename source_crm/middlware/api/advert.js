import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getAdvertsList', ...data}).then(r => r.data)
	},

	saveItem(data){
		return axios.post('', {action: 'insertAdvert', ...data}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'loadAdvert', id}).then(r => r.data)
	},

	copyItem(id){
		return axios.post('', {action: 'copyAdvert', copy: id}).then(r => r.data)
	},

	deleteItem(id){
		return axios.post('', {action: 'deleteAdvert', delete: id}).then(r => r.data)
	},

	moveItem(data){
		return axios.post('', data).then(r => r.data)
	},

	moderateSetItem(data){
		return axios.post('', {action: 'moderateAdvertItem', ...data}).then(r => r.data)
	},

	getModerateList(data){
		return axios.post('', {action: 'getModerateAdvertsList', ...data}).then(r => r.data)
	},

	getModerateItem(id){
		return axios.post('', {action: 'getModerateAdvertsItem', id}).then(r => r.data)
	},

	getModerateFilterList(){
		return axios.post('', {action: 'getModerateAdvertsFilter'}).then(r => r.data)
	},


}