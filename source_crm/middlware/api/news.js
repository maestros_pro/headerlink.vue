import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getNewsList', ...data}).then(r => r.data)
	},

	saveItem(data){
		return axios.post('', {action: 'insertNews', ...data}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'loadNews', id}).then(r => r.data)
	},

	copyItem(id){
		return axios.post('', {action: 'copyNews', copy: id}).then(r => r.data)
	},

	deleteItem(id){
		return axios.post('', {action: 'deleteNews', delete: id}).then(r => r.data)
	},

}