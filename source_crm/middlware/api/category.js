import axios from './instance'

export default {

	getSphere(){
		return axios.post('', {action: 'getSphere'}).then(r => r.data)
	},

	getMetro(){
		return axios.post('', {action: 'getMetro'}).then(r => r.data)
	},

	getRegion(){
		return axios.post('', {action: 'getRegion'}).then(r => r.data)
	},


}