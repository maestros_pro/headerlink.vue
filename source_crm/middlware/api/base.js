import axios from './instance'

export default {

	getKitCollect(){
		return axios.post('', {action: 'getKitCollect'}).then(r => r.data)
	},

	setKitCollect(data){
		return axios.post('', {action: 'setKitCollect', ...data})
	},

	getKitSetting(){
		return axios.post('', {action: 'getKitSetting'}).then(r => r.data)
	},

	setKitSetting(data){
		return axios.post('', {action: 'setKitSetting', ...data})
	},

	getFormData(){
		return axios.post('', {action: 'getFormData'}).then(r => r.data)
	},

	getSectionCreationCategoryTypes(){
		return axios.post('', {action: 'getSectionCreationCategoryTypes'}).then(r => r.data)
	},

	getCategory(data){
		return axios.post('', {action: 'getCategory', ...data}).then(r => r.data)
	},


}