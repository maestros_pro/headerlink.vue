import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getArticlesList', ...data}).then(r => r.data)
	},

	saveItem(data){
		return axios.post('', {action: 'insertArticle', ...data}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'loadArticle', id}).then(r => r.data)
	},

	copyItem(id){
		return axios.post('', {action: 'copyArticle', copy: id}).then(r => r.data)
	},

	deleteItem(id){
		return axios.post('', {action: 'deleteArticle', delete: id}).then(r => r.data)
	},


}