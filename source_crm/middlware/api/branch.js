import axios from './instance'

export default {

	getList(data){
		return axios.post('', {action: 'getBranchesList', ...data}).then(r => r.data)
	},

	saveItem(data){
		return axios.post('', {action: 'insertBranch', ...data}).then(r => r.data)
	},

	getItem(id){
		return axios.post('', {action: 'loadBranch', id}).then(r => r.data)
	},

	copyItem(id){
		return axios.post('', {action: 'copyBranch', copy: id}).then(r => r.data)
	},

	deleteItem(id){
		return axios.post('', {action: 'deleteBranch', delete: id}).then(r => r.data)
	},



}