import moment from 'moment'
import 'moment/locale/ru'
moment.locale('ru');
window['moment'] = moment;


function declension(value, args) {
	let cases = [2, 0, 1, 1, 1, 2],num = Number(value);
	if ( isNaN(num) ){
		console.error('declension must be a Number');
		return value;
	}
	else {
		return args[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
	}
}

const filter = {

	/**
	 *
	 * @param value - Number | String
	 * @param mark - String
	 * @returns {string}
	 *
	 * {{ '1000000000000' | discharge('*') }}
	 */
	discharge(value, mark = ' '){
		if (value === undefined) return '';
		return String(value).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, `$1${mark}`);
	},


	/**
	 *
	 * @param value - Number
	 * @param args - String
	 * @returns {string|*}
	 *
	 * {{ 100001 | declension('день', 'дня', 'дней') }}
	 */
	declension(value, ...args){
		if (!value) return '';
		let cases = [2, 0, 1, 1, 1, 2],num = Number(value);
		if ( isNaN(num) ){
			console.error('declension must be a Number');
			return value;
		}
		else {
			return args[(num%100>4 && num%100<20)? 2 : cases[(num%10<5)?num%10:5]];
		}
	},

	/**
	 *
	 * @param value - String
	 * @param limit - Number
	 * @param postfix - String
	 * @param minLastLetter
	 * @returns {string|*}
	 *
	 * {{ asdasdasdasd | substrLengh(10, '...') }}
	 */
	substrLengh(value, limit, postfix = '...', minLastLetter = 2){
		if ( !value ) return '';
		if ( !limit ) return value;
		let text = value.toString().trim(), arr, lastSpace;
		if( text.length <= limit) return text;
		text = text.slice( 0, limit);
		arr = text.split(' ');
		lastSpace = text.lastIndexOf(' ');
		if ( arr[arr.length - 1].length < minLastLetter && lastSpace > 0 ){
			text = text.substr(0, lastSpace);
		}
		return text + postfix;
	},



	passedTime(value, type){
		let date = new Date(value),
			month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октабря', 'ноября', 'декабря'],
			nowDate = new Date(),
			now = nowDate.getTime(),
			delta = now - date.getTime(),
			day = `${date.getDate().toString().length < 2 ? '0' + date.getDate().toString() : date.getDate().toString()}.${(date.getMonth() + 1).toString().length < 2 ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString()}.${date.getFullYear()}`,
			watch = `${date.getHours().toString().length < 2 ? '0' + date.getHours().toString() : date.getHours().toString() }:${date.getMinutes().toString().length < 2 ? '0' + date.getMinutes().toString() : date.getMinutes().toString() }`,
			time,
			message = watch
		;

		//console.info(value, date);

		if (type === 'textdate'){

			let D = date.getDate().toString(),
				M = month[date.getMonth()],
				Y = date.getFullYear()
			;

			message = `${D} ${M} ${nowDate.getFullYear() !== Y ? Y : ''}`;

		} else if (delta < 10000){
			message = `Только что`
		} else if (delta < 60000){ // менее минуты
			time = Math.round(delta/1000);
			message = `${time} ${declension(time, ['секунда', 'секунды', 'секунд'])} назад`
		} else if (delta < 3600000){ // менее часа
			time = Math.round(delta/60000);
			message = `${time} ${declension(time, ['минута', 'минуты', 'минут'])} назад`
		} else if (delta < 86400000){ //менее дня
			time = Math.round(delta/3600000);
			message = `${time} ${declension(time, ['час', 'часа', 'часов'])} назад`
		} else if (delta < 172800000){
			message = `Вчера, в ${watch}`
		} else if ( type === 'text' && delta < 31536000000  ){
			time = Math.round(delta/86400000);
			message = `${time} ${declension(time, ['день', 'дня', 'дней'])} назад`
		} else if ( type === 'text' ){
			message = `Больше года назад`
		} else if ( type === 'time') {
			message = `${day}, ${watch}`
		} else {
			message = `${day}`
		}

		return `${message}`;
	},

	dateDuration(value) {
		const date  = moment(value);
		const today = moment();
		const y     = moment.duration(today.diff(date)).years();
		const m     = moment.duration(today.diff(date)).months();
		const d     = moment.duration(today.diff(date)).days();
		let text    = '';

		if (y > 0) {
			text += `${y} ${filter.declension(y, 'год', 'года', 'лет')}`;
		}

		if (m > 0) {
			text += ` ${m} ${filter.declension(m, 'месяц', 'месяца', 'месяцев')}`;
		}

		if (d > 0) {
			text += ` ${d} ${filter.declension(d, 'день', 'дня', 'дней')}`;
		}

		return text;
	},


	phoneNumber(value) {
		const val = value.toString().replace(/[^\d]/gi, '');
		if (val.length === 11 && val.indexOf('7') === 0) {
			const match = val.match(/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/);
			return `+${match[1]} (${match[2]}) ${match[3]}-${match[4]}-${match[5]}`;
		} else {
			return value;
		}
	},

	clearTags(value) {
		if (!value) return '';
		let reg =  /<\/?[\w\s="/.':;#-\/]+>/gi,
			regend =  /<\/?[\w\s="/.':;#-\/]+$/gi
		;
		return value.replace(reg, '').replace(regend, '');
	},
};

export default filter;