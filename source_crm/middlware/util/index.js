
// remove empty parameters
export const clearObject = (o) => {
	let obj = {};

	Object.keys(o).map( i => {
		if ( !(o[i] === undefined || o[i] === '')) {
			obj[i] = o[i];
		}
	});

	return obj;
};

export const toFile = (dataurl, filename = 'file') => {
	let arr = dataurl.split(',');
	if ( !arr.length ) return;
	let mime = arr[0].match(/:(.*?);/)[1],
		bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
	while(n--) { u8arr[n] = bstr.charCodeAt(n) }
	return new File([u8arr], filename, {type : mime});
};


export const toBase64 = file => new Promise((resolve, reject) => {

	const reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = () => resolve(reader.result);
	reader.onerror = error => reject(error);
});

export const imageToBase64 = src => new Promise((resolve, reject) => {
	if ( !src || src.indexOf('data:image') === 0 ) {
		resolve('');
	} else {
		let xhr = new XMLHttpRequest();
		xhr.onload = ()=>{
			let reader = new FileReader();
			reader.onload = () => resolve(reader.result);
			reader.onerror = error => reject(error);
			reader.readAsDataURL(xhr.response);
		};
		xhr.onerror = error => reject(error);
		xhr.open('GET', src);
		xhr.responseType = 'blob';
		xhr.send();
	}

});
