import Vue from 'vue'
import Router from 'vue-router'


import Home from '../../view/home';
import Error from '../../view/error';
import Employees from '../../view/employees';
import Adverts from '../../view/adverts';
import Company from '../../view/company';
import Showcase from '../../view/showcase';
import Headings from '../../view/headings';
import Branches from '../../view/branches';
import News from '../../view/news';
import Dialogs from '../../view/dialogs';
import Articles from '../../view/articles';
import Notebook from '../../view/notebook';
import Balance from '../../view/balance';
import Support from '../../view/support';
import Notifications from '../../view/notifications';

import Map from '../../view/map';
import Reserve from '../../view/reserved.vue';

//test
import Components from '../../view/example';
import Popups from '../../view/popups';

Vue.use(Router);

const routes = [
	{
		path: '/',
		component: Home,
		name: 'home',
		meta: {
			title: 'Моя компания',
			group: 'home'
		}
	},
	{
		path: '/company',
		component: Company,
		name: 'company',
		meta: {
			title: 'Моя компания',
			group: 'home'
		}
	},
	{
		path: '/adverts',
		component: Adverts,
		name: 'adverts',
		meta: {
			title: 'Объявления',
			group: 'adverts'
		}
	},
	{
		path: '/adverts/add',
		component: Adverts,
		name: 'advertsAdd',
		meta: {
			title: 'Объявления',
			group: 'adverts'
		}
	},
	{
		path: '/adverts/:id',
		component: Adverts,
		name: 'advertsItem',
		meta: {
			title: 'Объявления',
			group: 'adverts'
		}
	},
	{
		path: '/headings',
		component: Headings,
		name: 'headings',
		meta: {
			title: 'Заголовки',
			group: 'headings'
		}
	},
	{
		path: '/dialogs',
		component: Dialogs,
		name: 'dialogs',
		meta: {
			title: 'Диалоги',
			group: 'dialogs'
		}
	},
	{
		path: '/branches',
		component: Branches,
		name: 'branches',
		meta: {
			title: 'Филиалы',
			group: 'branches'
		}
	},
	{
		path: '/branches/add',
		component: Branches,
		name: 'branchesAdd',
		meta: {
			title: 'Филиалы',
			group: 'branches'
		}
	},
	{
		path: '/branches/:id',
		component: Branches,
		name: 'branchesItem',
		meta: {
			title: 'Филиалы',
			group: 'branches'
		}
	},
	{
		path: '/news',
		component: News,
		name: 'news',
		meta: {
			title: 'Новости',
			group: 'news'
		}
	},
	{
		path: '/news/add',
		component: News,
		name: 'newsAdd',
		meta: {
			title: 'Новости',
			group: 'news'
		}
	},
	{
		path: '/news/:id',
		component: News,
		name: 'newsItem',
		meta: {
			title: 'Новости',
			group: 'news'
		}
	},
	{
		path: '/employees',
		component: Employees,
		name: 'employees',
		meta: {
			title: 'Сотрудники',
			group: 'employees'
		}
	},
	{
		path: '/articles',
		component: Articles,
		name: 'articles',
		meta: {
			title: 'Статьи',
			group: 'articles'
		}
	},
	{
		path: '/articles/add',
		component: Articles,
		name: 'articlesAdd',
		meta: {
			title: 'Статьи',
			group: 'articles'
		}
	},
	{
		path: '/articles/:id',
		component: Articles,
		name: 'articlesItem',
		meta: {
			title: 'Статьи',
			group: 'articles'
		}
	},
	{
		path: '/notifications',
		component: Notifications,
		name: 'notifications',
		meta: {
			title: 'Оповещения',
			group: 'notifications'
		}
	},
	{
		path: '/balance',
		component: Balance,
		name: 'balance',
		meta: {
			title: 'Баланс',
			group: 'company'
		}
	},
	{
		path: '/showcase',
		component: Showcase,
		name: 'showcase',
		meta: {
			title: 'Витрина',
			group: 'showcase'
		}
	},
	{
		path: '/showcase/add',
		component: Showcase,
		name: 'showcaseAdd',
		meta: {
			title: 'Витрина',
			group: 'showcase'
		}
	},
	{
		path: '/showcase/:id',
		component: Showcase,
		name: 'showcaseItem',
		meta: {
			title: 'Витрина',
			group: 'showcase'
		}
	},
	{
		path: '/notebook',
		component: Notebook,
		name: 'notebook',
		meta: {
			title: 'Записная книга',
			group: 'notebook'
		}
	},


	{
		path: '/support',
		component: Support,
		name: 'support',
		meta: {
			title: 'Техподдержка',
			group: 'support'
		}
	},


	{
		path: '/map',
		component: Map,
		meta: {
			title: 'Карта сайта'
		}
	},

	// test
	{
		path: '/components',
		component: Components,
		meta: {
			title: 'Сomponents example'
		}
	},
	{
		path: '/popups',
		component: Popups,
		meta: {
			title: 'Попапы'
		}
	},

	{
		path: '*/*',
		component: Error,
		meta: {
			title: 'Страница не существует'
		}
	}
];

const router = new Router({
	mode: isProd ? 'history' : 'hash',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});


router.afterEach((to, from) => {
	document.title = to.meta.title;
});

export default router