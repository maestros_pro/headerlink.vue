import eventHub from './eventhub';
import OverlayScrollbars from 'overlayscrollbars';
import VTooltip from 'v-tooltip'
import HighchartsVue from 'highcharts-vue';
import VueDragDrop from 'vue-drag-drop';
import VueTextareaAutosize from 'vue-textarea-autosize';
import YmapPlugin from 'vue-yandex-maps'
import VueScroll from 'vuescroll';
import OverlayScrollbarsChat from './OverlayScrollbarsChat';
import VueDragscroll from 'vue-dragscroll'
import moment from './moment/index';
import lazyload from 'vue-lazyload';
import VueAwesomeSwiper from 'vue-awesome-swiper';

export default {
	VueDragscroll: {
		plugin: VueDragscroll,
		options: {}
	},

	VueAwesomeSwiper: {
		plugin: VueAwesomeSwiper
	},

	lazyload: {
		plugin: lazyload,
		options: {
			preLoad: 1.3,
			//error: require('../../../img/svg/btn-loader.svg'),
			//loading: require('../../img/svg/loader-yellow.svg'),
			attempt: 1
		}
	},
	eventHub: {
		plugin: {
			install: V => {
				V.prototype.$eventHub = eventHub;
			}
		},
		options: {}
	},

	moment: {
		plugin: {
			install: V => {
				V.prototype.$moment = moment;
			}
		}
	},
	VueScroll: {
		plugin: VueScroll,
		options: {
			ops: {
				vuescroll: {
					sizeStrategy: 'number',
					detectResize: true,
				},

				scrollPanel: {
					scrollingX: false,
				},
				rail: {
					background: '#cccccc',
					// opacity: 0,
					size: '6px',
					specifyBorderRadius: false,
					gutterOfEnds: '5px',
					gutterOfSide: '10px',
					keepShow: false
				},
				bar: {
					showDelay: 500,
					onlyShowBarOnScroll: true,
					keepShow: true,
					background: '#cccccc',
					opacity: 1
				}
			}

		}
	},
	tooltip: {
		plugin: VTooltip,
		options: {}
	},
	highcharts: {
		plugin: HighchartsVue,
		options: {}
	},
	dragndrop: {
		plugin: VueDragDrop,
		options: {}
	},
	textareaAutosize: {
		plugin: VueTextareaAutosize,
		options: {}
	},
	YmapPlugin: {
		plugin: YmapPlugin,
		options: {
			apiKey: window.ymapsApiKey || 'fb22bfdd-4c00-4598-9a19-eba6ba33b360',
			lang: 'ru_RU',
			version: '2.1'
		}
	},

	scrollbarChat: {
		plugin: {
			install: (V)=>{
				V.directive('scrollchat', {
					inserted(el, binding, vnode) {
						let options = {
							className: 'os-theme-thin-light',
							sizeAutoCapable : true,
							paddingAbsolute : true,
							scrollbars : {
								clickScrolling : true,
								autoHide : "leave"
							},
							overflowBehavior:{
								x: 'hidden'
							}
						};
						if (!el.scrollbar) {
							el.scrollbar = OverlayScrollbars(el, Object.assign(options, binding.value));
							el.scrollbar.addExt('OverlayScrollbarsChat');
						}
					},

					unbind(el, binding, vnode){
						if (el.scrollbar) el.scrollbar.destroy();
					}
				});
			}
		},
	},

	scrollbar: {
		plugin: {
			install: (V)=>{
				V.directive('scrollbar', {
					inserted(el, binding, vnode) {
						let options = {
							className: 'os-theme-thin-light',
							sizeAutoCapable : true,
							paddingAbsolute : true,
							scrollbars : {
								clickScrolling : true,
								autoHide : "leave"
							},
							overflowBehavior:{
								x: 'hidden'
							}
						};
						if (!el.scrollbar) el.scrollbar = OverlayScrollbars(el, Object.assign(options, binding.value));
					},

					unbind(el, binding, vnode){
						if (el.scrollbar) el.scrollbar.destroy();
					}
				});
			}

		},
		options: {
		}
	},
}