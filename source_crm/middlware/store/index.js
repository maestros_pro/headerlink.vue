import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import user from './user'
import base from './base'
import advert from '@views/adverts/store'
import category from './category'
import news from '@views/news/store'
import article from '@views/articles/store'
import branch from '@views/branches/store'
import finance from '@views/finance/store'
import showcase from '@views/showcase/store'
import rules from '@component/moderate/rules'


export default new Vuex.Store({
	modules: {
		user,
		base,
		advert,
		category,
		news,
		article,
		branch,
		rules,
		finance,
		showcase,
	},
})


