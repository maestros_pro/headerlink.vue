import {base as Api} from '@api'

function listToTree(data, options){
	options = options || {};
	const ID_KEY = options.idKey || 'id';
	const PARENT_KEY = options.parentKey || 'parent';
	const CHILDREN_KEY = options.childrenKey || 'children';

	let tree = [], childrenOf = {};
	let item, id, parentId;

	for(let i = 0, length = data.length; i < length; i++) {
		item = data[i];
		id = item[ID_KEY];
		parentId = item[PARENT_KEY] || 0;
		childrenOf[id] = childrenOf[id] || [];
		item[CHILDREN_KEY] = childrenOf[id];
		if (parentId !== 0) {
			childrenOf[parentId] = childrenOf[parentId] || [];
			childrenOf[parentId].push(item);
		} else {
			tree.push(item);
		}
	}

	return tree;
}

export default {
	namespaced: true,

	state: {

		aside: false,

		accountMenu: false,

		kit: {
			home: [[],[]],
			settings: [],
			list: [],
		},

		selects: {
			companyLegal: [],
			advertType: [],
			advertCount: [],
			branchType: [],
		},

		settings: {
			advertTitleLenght: 100,
			newsTitleLenght: 100,
			articleTitleLenght: 100,
		},


		sectionCreationCategoryTypes: [],

		sectionCreationCategory: [],

	},

	mutations: {
		SET_KIT_COLLECTION(state, value) {
			state.kit.home = [...value];
		},

		SET_KIT_SETTINGS(state, value) {
			state.kit.list = [...value];
			let checked = [];
			state.kit.list.map( i => { if (i.active) checked.push(i.id) });
			state.kit.settings = [...checked];
		},

		PROP_KIT_GRID(state, { column, value }) {
			let kit = [...state.kit.home];
			kit[column] = value;
			state.kit.home = kit;
		},

		PROP_KIT_SETTINGS(state, value) {
			state.kit.settings = value;
		},

		PROP_KIT_TOGGLE(state, { column, index, data }) {
			let collection = state.kit.home;
			if ( data.value ){
				collection[column][index][data.name] = data.value;
			} else {
				collection[column][index][data.name] = !collection[column][index][data.name];
			}

			state.kit.home = [...collection];

		},

		PROP_FORM_SETTINGS(state, data) {
			Object.keys(data).map( name => {

				if (name === 'settings') {
					Object.keys(state[name]).map( name1 => {
						state.settings[name1] = data[name][name1];
					});
				} else {
					state.selects[name] = [...data[name]];
				}
			});
		},

		TOGGLE_ASIDE (state){
			state.aside = !state.aside;
		},

		TOGGLE_ACCOUNT_MENU (state, value){
			state.accountMenu = value !== undefined ? value : !state.accountMenu;
		},

		SET_CREATE_SECTION_TYPES(state, {items}){
			state.sectionCreationCategoryTypes = items;
		},

		SET_CREATE_SECTION_CATEGORIES(state, items){
			if (!items) return [];
			state.sectionCreationCategory = listToTree(items, {parentKey: 'parent_id'});
		}
	},

	actions: {

		getKitCollect({commit}){
			return Api.getKitCollect()
				.then(({list})=>{
					commit("SET_KIT_COLLECTION", list);
				})
			;
		},

		setKitCollect({state}){
			return Api.setKitCollect({kits: JSON.stringify(state.kit.home)});
		},

		getKitSetting({commit}){
			return Api.getKitSetting()
				.then(({list})=>{
					commit("SET_KIT_SETTINGS", list);
				})
			;
		},

		setKitSetting({state}) {
			return Api.setKitSetting({kits: JSON.stringify(state.kit.settings)});
		},

		propKitGrid({commit, state}, data) {
			commit("PROP_KIT_GRID", data);
		},

		propKitToggle({commit, state}, data) {
			commit("PROP_KIT_TOGGLE", data);
		},

		propKitSettinds({commit, state}, data) {
			commit("PROP_KIT_SETTINGS", data);
		},

		getFormData({commit}) {
			return Api.getFormData()
				.then((data)=>{
					commit("PROP_FORM_SETTINGS", data);
				})
			;
		},

		async getSectionCreationCategoryTypes({commit}, type){
			const value = await Api.getSectionCreationCategoryTypes(type);
			commit("SET_CREATE_SECTION_TYPES", value);
		},

		async getCategory({commit}, data){
			const value = await Api.getCategory(data);
			commit("SET_CREATE_SECTION_CATEGORIES", value.category);
		},
	},

	getters: {
		sectionCreationCategoryTypes: state => section => {
			return state.sectionCreationCategoryTypes.find( i => i.section === section);
		},
	}
}