import {category as Api} from '@api'


export default {
	namespaced: true,

	state: {
		sphere: {
			items: [],
			deep: 4,
			name: 'Сфера деятельности',
		},

		metro: {
			items: [],
			deep: 3,
			name: 'Метро',
		},

		region: {
			items: [],
			deep: 2,
			name: 'Регион деятельности',
		},
	},

	mutations: {
		SET_STATE(state, {name, value}){
			state[name].items = [...value];
		}
	},

	actions: {
		getSphere({commit}){
			return Api.getSphere()
				.then(({sphere})=>{
					commit("SET_STATE", { name: 'sphere', value: sphere});
				})
			;
		},

		getMetro({commit}){
			return Api.getMetro()
				.then(({metro})=>{
					commit("SET_STATE", { name: 'metro', value: metro});
				})
			;
		},

		getRegion({commit}){
			return Api.getRegion()
				.then(({region})=>{
					commit("SET_STATE", { name: 'region', value: region});
				})
			;
		},

	},

	getters: {

		group: state => (type, level, parent_id) => {
			return state[type].items.filter( i => i.level === level && i.parent_id === parent_id ) || {}
		},

		item: state => (type, id) => {
			return state[type].items.find( i => i.id === id ) || {}
		},

		byName: state => (type, name, level) => {
			return state[type].items.find( i => i.name.toLowerCase() === name.toLowerCase() && ( level ? i.level === level : true ) ) || {}
		},

		deep: state => (type) => {
			return state[type].deep
		},

		hasMetro: state => (name) => {
			return name && state.metro.items.filter(i => i.level === 1).find( i => {
				return name.toLowerCase() === i.name.toLowerCase();
			})
		},

		counter: state => (type, model, id) => {
			let obj = {};

			model.map(id => {
				addObj(id);
			});

			return obj[id] || 0;

			function addObj(id) {
				let item = state[type].items.find( point => point.id === id );
				obj[id] = obj[id] ? obj[id] + 1 : 1;

				if ( item && item.parent_id ){
					addObj(item.parent_id)
				}
			}
		}

	}
}