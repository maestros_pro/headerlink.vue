import {user as Api} from '@api'

import {toFile, imageToBase64} from "@util";

function parseDefaultCompany(data) {
	return {
		company_id: data.company_id || null,
		image: data.image || '',
		date_added: data.date_added || new Date(),
		image_original: data.image_original || '',
		image_coordinates: data.image_coordinates || '',
		category: data && data.category_company ? data.category_company.map(i => i.category_id ) : [],
		region: data && data.region_company ? data.region_company.map(i => i.zone_id ) : [],
		name: data.name || '',
		forma_id: data.forma_id || '',
		inn: data.inn || '',
		kpp: data.kpp || '',
		phone_prefix: data.phone_prefix || '',
		phone: data.phone || '',
		phone_dob: data.phone_dob || '',
		email: data.email || '',
		email_customer: !!data.email_customer,
		site: data.site || '',
		yur_address: data.yur_address || '',
		bank_name: data.bank_name || '',
		bank_bik: data.bank_bik || '',
		ogrn: data.ogrn || '',
		rs: data.rs || '',
		ks: data.ks || '',
		fio: data.fio || '',
		description: data.description || '',
	};
}

export default {
	namespaced: true,

	state: {

		mainDataLoaded: false,

		allData: {
			user_company: [],
			user_info: [],
		},

		info: {},

		company: {},

		moderate: {},

	},

	mutations: {
		SET_USER_DATA(state, data){
			let info = data && data.user_info && data.user_info.length ? data.user_info[0] : {};
			let company = data && data.user_company && data.user_company.length && data.user_company[0] ? data.user_company[0] : {};

			if ( data.moderate ) state.moderate = data.moderate;

			if ( !company.image_original || !company.image) {
				state.info = info;
				state.mainDataLoaded = true;
				state.allData = {
					user_info: [info],
					user_company: [company]
				};
				state.company = JSON.parse(JSON.stringify(Object.assign({}, parseDefaultCompany(company))));
				console.info(state.company)
				return false;
			}

			Promise.all([imageToBase64(company.image_original), imageToBase64(company.image)])
				.then(res => {
					company.image_original = res[0];
					company.image = res[1];
				})
				.finally(()=>{
					state.info = info;
					state.mainDataLoaded = true;
					state.allData = {
						user_info: [info],
						user_company: [company]
					};
					state.company = JSON.parse(JSON.stringify(Object.assign({}, parseDefaultCompany(company))));
				})
			;

		},

		SET_PROP(state, { name, value }) {
			state.company[name] = value;
		},
	},

	actions: {
		getData({commit}){
			commit("SET_USER_DATA", {});
			return Api.getData()
				.then((data)=>{
					commit("SET_USER_DATA", data);
				})
			;
		},

		saveCompany({state}){

			let obj = JSON.parse(JSON.stringify(state.company));

			if (obj.image_original) obj.image_original = toFile(state.company.image_original);
			if (obj.image) obj.image = toFile(state.company.image);

			return Api.saveCompany(obj);
		},

		updateUserImage({state}){

			return Api.updateUserImage({
				image: state.company.image ? toFile(state.company.image) : state.company.image,
				image_original: state.company.image_original ? toFile(state.company.image_original) : state.company.image_original,
				image_coordinates: state.company.image_coordinates,
			});
		},
	},

	getters: {
		mainDataLoaded: state => {
			return state.mainDataLoaded;
		},

		companyChanged: state => {
			let changed = false;

			if ( !(state.allData && state.allData.user_company && state.allData.user_company.length && state.allData.user_company[0]) ) return true;

			const baseData = JSON.parse(JSON.stringify(Object.assign({}, parseDefaultCompany(state.allData.user_company[0]))));

			Object.keys(state.company).map( item =>{
				if (baseData[item] !== null && baseData[item].toString() !== state.company[item].toString()) changed = true;
			});

			return changed
		}
	}
}