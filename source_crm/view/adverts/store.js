import {advert as Api} from '@api'
import {clearObject} from '@util'

export default {
	namespaced: true,

	state: {
		item: {},

		_source: {},

		_item: {
			advert_title: '',
			advert_theme: [],
			advert_text: '',
			advert_link: '',
			advert_price: '',
			advert_amount: '',
			advert_type: '',
			advert_branch: [],
			advert_gallery: [],
			advert_category_types: [],
			advert_video: '',
			advert_exchange: false
		},

		collection: {
			currentGroup: false,
			count: 0,
			groups: [],
			list: [],
		},

		filter: {
			limit: 10,
			offset: 0,
			group: '',
			query: '',
		},

		moderateLoading: false,

		moderateItem: {
			id: null,
			list: [],
			title: '',
		},

		moderateFilter: {},

		moderateFilterList: null
	},

	mutations: {
		SET_LIST(state, value) {
			if ( value === null ){
				state.collection = null;
			} else {
				state.collection = JSON.parse(JSON.stringify(value));
			}
		},

		SET_ITEM(state, data = {}) {
			state._source = JSON.parse(JSON.stringify(Object.assign({}, state._item, data)));;
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state._item, data)));
		},

		SET_FILTER(state, {name, value}) {
			state.filter[name] = value;
		},

		SET_PROP(state, { name, value }) {
			state.item[name] = value;
		},

		SET_MODERATE_FILTER(state, value) {
			state.moderateFilterList = value;
		},

		SET_MODERATE_ITEM(state, {title, id, list}) {
			state.moderateItem.title = title || '';
			state.moderateItem.id = id || null;
			state.moderateItem.list = list || [];
		},

		SET_MODERATE_LOADING(state, value) {
			state.moderateLoading = value;
		},

		SET_MODERATE_FILTER_PROPS(state, value) {
			state.moderateFilter = value;
		},

		APPROVE_MODERATE_POINT(state, id) {
			const idx = state.moderateItem.list.findIndex(item => item.id === id);
			if ( idx >= 0) {
				const obj = JSON.parse(JSON.stringify(state.moderateItem.list[idx]));
				obj.approved = true;
				obj.offense = [];
				state.moderateItem.list = [...state.moderateItem.list.slice(0, idx), obj, ...state.moderateItem.list.slice(idx + 1)];
			}
		},

		REJECT_MODERATE_POINT(state, {id, value}) {
			const idx = state.moderateItem.list.findIndex(item => item.id === id);
			if ( idx >= 0) {
				const obj = JSON.parse(JSON.stringify(state.moderateItem.list[idx]));
				obj.approved = false;
				obj.offense = value;
				state.moderateItem.list = [...state.moderateItem.list.slice(0, idx), obj, ...state.moderateItem.list.slice(idx + 1)];
			}
		},
	},

	actions: {

		getList({commit, state}){
			return Api.getList(clearObject(state.filter))
				.then((res)=>{
					commit("SET_LIST", res);
				})
			;
		},

		getModerateList({commit, state}){
			return Api.getModerateList({...clearObject(state.filter), ...clearObject(state.moderateFilter)})
				.then((res)=>{
					commit("SET_LIST", res);
				})
			;
		},

		getModerateFilterList({commit, state}){
			return Api.getModerateFilterList()
				.then((res)=>{
					commit("SET_MODERATE_FILTER", res.filter);
				})
			;
		},

		getItem({commit, state}, id){
			commit("SET_ITEM");

			if ( !id ) return false;

			return Api.getItem(id)
				.then((data)=>{
					commit("SET_ITEM", {...data, id});
				})
			;

		},

		getModerateItem({commit, state}, id){
			commit("SET_MODERATE_LOADING", true);

			if ( !id ) return false;

			return Api.getModerateItem(id)
				.then((data)=>{
					commit("SET_MODERATE_ITEM", {...data, id});
				}).finally(()=>{
					commit("SET_MODERATE_LOADING", false);
				})
			;
		},

		async moderateSetItem({dispatch, commit, state}, query){

			const data = state.moderateItem.list.map( item => {
				let rules = item.offense && item.offense.length ? item.offense.map( i => i.id) : [];
				let obj = {
					id: item.id
				};
				if ( item.approved === true ) obj.approved = true;
				if ( item.approved === false ) obj.rejected = true;
				if ( rules.length ) obj.rules = rules;
				return obj;
			})
			//-console.info(data);

			return await Api.moderateSetItem({
				id: state.moderateItem.id,
				items: data.map(item => {
					return JSON.stringify(item);
				}),
				...query
			});
		},


		saveItem({state}, data = {}){
			return Api.saveItem({...state.item, ...data});
		},

		copyItem({commit}, id){
			return Api.copyItem(id);
		},

		deleteItem({commit}, id){
			return Api.deleteItem(id);
		},

		moveItem({commit}, data){
			return Api.moveItem(data);
		},

	},

	getters: {

		changed: state => {
			let changed = false;

			Object.keys(state.item).map( item =>{
				if (state._source[item] !== null && state._source[item].toString() !== state.item[item].toString()) {
					changed = true;
				}
			});

			return changed
		}
	}
}