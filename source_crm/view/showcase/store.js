import {showcase as Api} from '@api'
import {clearObject} from '@util'

export default {
	namespaced: true,

	state: {
		item: {},

		_source: {},

		_item: {
			showcase_title: '',
			showcase_theme: [],
			showcase_text: '',
			showcase_link: '',
			showcase_price: '',
			showcase_amount: '',
			showcase_type: '',
			showcase_branch: [],
			showcase_gallery: [],
			showcase_category_types: [],
			showcase_video: '',
			showcase_exchange: false
		},

		collection: {
			adverts: {
				free: 0,
				buzy: 100
			},
			currentGroup: false,
			count: 0,
			groups: [],
			list: [],
		},

		filter: {
			limit: 10,
			offset: 0,
			group: '',
			query: '',
		},
	},

	mutations: {
		SET_LIST(state, value) {
			if ( value === null ){
				state.collection = null;
			} else {
				state.collection = JSON.parse(JSON.stringify(value));
			}
		},

		SET_ITEM(state, data = {}) {
			state._source = JSON.parse(JSON.stringify(Object.assign({}, state._item, data)));;
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state._item, data)));
		},

		SET_FILTER(state, {name, value}) {
			state.filter[name] = value;
		},

		SET_PROP(state, { name, value }) {
			state.item[name] = value;
		},

	},

	actions: {

		getList({commit, state}){
			return Api.getList(clearObject(state.filter))
				.then((res)=>{
					commit("SET_LIST", res);
				})
			;
		},

		getItem({commit, state}, id){
			commit("SET_ITEM");

			if ( !id ) return false;

			return Api.getItem(id)
				.then((data)=>{
					commit("SET_ITEM", {...data, id});
				})
			;

		},

		saveItem({state}, data = {}){
			return Api.saveItem({...state.item, ...data});
		},

		copyItem({commit}, id){
			return Api.copyItem(id);
		},

		deleteItem({commit}, id){
			return Api.deleteItem(id);
		},

	},

	getters: {

		changed: state => {
			let changed = false;

			Object.keys(state.item).map( item =>{
				if (state._source[item] !== null && state._source[item].toString() !== state.item[item].toString()) {
					changed = true;
				}
			});

			return changed
		}
	}
}