import {finance as Api} from '@api'
import $hub from '@library/eventhub'


export default {
	namespaced: true,

	state: {
		kit: {
			group: '',
			currency: '',

			groupList: [],
			currencyList: [],

			chart: []

		},

		inAside: [],

		quotes: [],

		settings: {
			data: [],
			model: [],
			loaded: false
		},

		itemChart: {
			title: '',
			data: []
		},
	},

	mutations: {
		SET_KIT(state, value) {
			state.kit.group = value.currentGroup || '';
			state.kit.currency = value.currentCurrency || '';
			state.kit.groupList = value.groups || [];
			state.kit.currencyList = value.currencyList || [];
			state.kit.chart = value.chart || [];
		},

		SET_ASIDE_COURSE(state, data){
			state.inAside = [...data.main] || [];
			state.quotes = [...data.list] || [];
		},

		SET_KIT_PROP(state, data){
			state.kit[data.name] = data.value;
		},

		SET_COURSE_SETTINGS(state, {settings}){
			state.settings.data = settings;
			let model = []
			state.settings.data.forEach( item => {
				model.push({
					cources: item.options.tabs.value,
					accuracy: item.options.accuracy.value,
					higlight: item.options.higlight,
				});
			});

			state.settings.model = JSON.parse(JSON.stringify(model));

			state.settings.loaded = true;

		},

		SET_COURSE_SETTINGS_PROP(state, {index, name, value}){
			$hub.$set(state.settings.model[index], name, value)
		},

		SET_ITEM_CHART(state, data){
			state.itemChart.data = data.chart ? data.chart.data : [];
			state.itemChart.title = data.title;
		},

	},

	actions: {

		async getFinanceKit({commit, state}){
			const data = {
				group: state.kit.group,
				currency: state.kit.currency,
			};
			const value = await Api.getFinanceKit(data);

			commit('SET_KIT', value)
		},

		async getFinanceCources({commit}){
			const value = await Api.getFinanceCources();
			commit('SET_ASIDE_COURSE', value)
		},

		async getFinanceSettings({commit}){
			const value = await Api.getFinanceSettings();
			commit('SET_COURSE_SETTINGS', value)
		},

		async saveFinanceSettings({state}){
			let data = {};

			state.settings.model.forEach((item, idx) => {
				let section = state.settings.data[idx].value;
				data[`${section}_cources`] = item.cources;
				data[`${section}_accuracy`] = item.accuracy;
				data[`${section}_higlight`] = item.higlight;
			})


			await Api.saveFinanceSettings(data);
		},

		async getFinanceItemChart({commit}, id){
			const value = await Api.getFinanceItemChart(id);
			commit('SET_ITEM_CHART', value)
			return value;
		},

	},

	getters: {
		settingsChange: state => {
			let changed = false;

			if (!state.settings.model.length) return false;

			state.settings.model.forEach((item, index) => {
				let options = state.settings.data[index].options,
					baseAccuracy = JSON.stringify(options.accuracy.value),
					baseCources = JSON.stringify(options.tabs.value),
					baseHiglight = JSON.stringify(options.higlight),
					currentAccuracy = JSON.stringify(item.accuracy),
					currentCources = JSON.stringify(item.cources),
					currentHiglight = JSON.stringify(item.higlight)
				;

				if ( baseAccuracy !== currentAccuracy
					|| baseCources !== currentCources
					|| baseHiglight !== currentHiglight) {
					changed = true;
				}
			});

			return changed
		}
	}
}