import {branch as Api} from '@api'
import {clearObject} from '@util'

export default {
	namespaced: true,

	state: {
		item: {},

		_source: {},

		_item: {
			branch_title: '',
			branch_coordinates: '',
			branch_type: '',
			address_all: '',
			address_country: '',
			address_city: '',
			address_street: '',
			address_house: '',
			address_part: '',
			address_lit: '',
			address_room: '',
			branch_metro: [],
			phone_number: [],
			branch_email: '',
			branch_site: '',
		},

		collection: {
			currentGroup: false,
			count: 0,
			groups: [],
			list: [],
		},

		filter: {
			limit: 10,
			offset: 0,
			group: '',
			query: '',
		}
	},

	mutations: {
		SET_LIST(state, value) {
			if ( value === null ){
				state.collection = null;
			} else {
				state.collection = JSON.parse(JSON.stringify(value));
			}
		},

		SET_ITEM(state, data = {}) {
			let obj = {};

			Object.keys(data).map( name => {
				if ( !!data[name] ) obj[name] = data[name];
			});

			state._source = JSON.parse(JSON.stringify(Object.assign({}, state._item, obj)));
			state.item = JSON.parse(JSON.stringify(Object.assign({}, state._item, obj)));
		},

		SET_FILTER(state, {name, value}) {
			state.filter[name] = value;
		},

		SET_PROP(state, { name, value }) {
			state.item[name] = value;
		},
	},

	actions: {

		getList({commit, state}){
			return Api.getList(clearObject(state.filter))
				.then((res)=>{
					commit("SET_LIST", res);
				})
			;
		},

		getItem({commit, state}, id){
			commit("SET_ITEM");

			if ( !id ) return false;

			return Api.getItem(id)
				.then((data)=>{
					commit("SET_ITEM", {...data, id});
				})
			;

		},

		saveItem({state}, data = {}){
			return Api.saveItem({...state.item, ...data});
		},

		copyItem({commit}, id){
			return Api.copyItem(id);
		},

		deleteItem({commit}, id){
			return Api.deleteItem(id);
		},

		moveItem({commit}, data){
			return Api.moveItem(data);
		},

	},

	getters: {

		changed: state => {
			let changed = false;

			Object.keys(state.item).map( name =>{
				if (state._source[name] !== null && state.item[name] !== null && state._source[name] !== undefined && state.item[name] !== undefined && state._source[name].toString() !== state.item[name].toString()) {
					changed = true;
				}
			});

			return changed
		}
	}
}