'use strict';

export default class Form {
	constructor(options){
		this._polyfill();
		Object.assign(this._options = {}, this._default(), options);
	}

	_polyfill() {

		//assign
		if (typeof Object.assign !== 'function') {
			Object.assign = function (target, varArgs) { // .length of function is 2
				'use strict';
				if (target === null) { // TypeError if undefined or null
					throw new TypeError('Cannot convert undefined or null to object');
				}

				let to = Object(target);

				for (let index = 1; index < arguments.length; index++) {
					let nextSource = arguments[index];

					if (nextSource !== null) { // Skip over if undefined or null
						for (let nextKey in nextSource) {
							// Avoid bugs when hasOwnProperty is shadowed
							if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
				}
				return to;
			};
		}

	}

	_default() {
		return {
			fields: {
				file: {
					empty: {
						message: 'Добавьте файл'
					}
				},
				radio: {
					unchecked: {
						message: 'Отметьте один из вариантов'
					}
				},
				default: {
					empty: {
						message: 'Заполните поле'
					}
				},
				email: {
					empty: {
						message: 'Заполните поле'
					},
					value: [
						{
							val: 'test@test.test',
							message: 'Какая, блин, оригинальная почта...'
						}
					],
					pattern: [
						{
							regex: /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/i,
							message: 'Проверьте корректность E-mail'
						}
					],
					antiPattern: [
						{
							regex: 'test@test.test',
							message: 'Какая, блин, оригинальная почта...'
						}
					]
				},
				email_n: {
					value: [
						{
							val: 'test@test.test',
							message: 'Какая, блин, оригинальная почта...'
						}
					],
					pattern: [
						{
							regex: /^[-._a-z0-9а-я]+@(?:[a-z0-9а-я][-a-z0-9а-я]+\.)+[a-zа-я]{2,6}$/i,
							message: 'Проверьте корректность E-mail'
						}
					],
					antiPattern: [
						{
							regex: 'test@test.test',
							message: 'Какая, блин, оригинальная почта...'
						}
					]
				},
				password: {
					empty: {
						message: 'Заполните поле'
					},
					// min:{
					// 	length: 0,
					// 	message: 'Длина пароля должна быть не менее 6 символов'
					// },
					// max: {
					// 	length: 999,
					// 	message: 'Длина пароля не должна превышать 999 символов'
					// },
				},
				number: {
					empty: {
						message: 'Заполните поле'
					},
					pattern: [
						{
							regex: /^\d+$/i,
							message: 'Поле должно содержать только числовые символы'
						}
					],
				},
				checkbox: {
					unchecked: {
						message: 'Надо тут чекнуть'
					}
				}
			}
		}
	}

	validate(input, type, result) {
		if ( !type || type === 'true' || type === true ) {
			if ( input.type === 'file' || input.type === 'radio' || input.type === 'checkbox' || input.type === 'email' || input.type === 'password' ){
				type = input.type;
			} else {
				type = 'default';
			}
		}

		let err = [], val, rules = this._options.fields[type];

		//-console.info(type, input, input.value, input.files);

		val = input.value.trim();

		if ( !!rules ){
			for (let rule in rules) {
				if (rules.hasOwnProperty(rule)) {
					switch (rule){
						case 'empty':
							if ( val === '' ) err.push(rules[rule].message);
							break;
						case 'pattern':
							if ( val && rules[rule].length ) rules[rule].map(item=>{
								if ( item.regex && item.regex instanceof RegExp && !item.regex.test(val) ) err.push(item.message);
							});
							break;
						case 'antiPattern':
							if ( val && rules[rule].length ) rules[rule].map(item=>{
								if ( item.regex && item.regex instanceof RegExp && item.regex.test(val) ) {
									err.push(item.message);
								} else if ( item.regex && typeof item.regex === 'string' && val === item.regex ){
									err.push(item.message);
								}
							});
							break;
						case 'min':
							if ( val && val.length < rules[rule].length ) err.push(rules[rule].message);
							break;
						case 'max':
							if ( val && val.length > rules[rule].length ) err.push(rules[rule].message);
							break;
						case 'unchecked':
							if ( type === 'radio' ){
								let i = 0, radio = document.querySelectorAll('input[name=' + input.name + ']'), checked = false;

								for (i; i < radio.length; i++ ){
									if ( radio[i].checked ) {
										checked = true;
										break;
									}
								}
								if ( !checked ) err.push(rules[rule].message);

							} else if ( !input.checked ) {
								err.push(rules[rule].message);
							}
							break;
						default:

					}
				}
			}
		}

		if (result && typeof result === 'function') result({errors: err, value: val, type: type});
	}

}