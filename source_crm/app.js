import style from './css/style.scss';

import './img/svg/trumbowyg.svg';

import Vue from 'vue';
import router from './middlware/router'
import './js/modules/module.helper.js'

import Api from './js/modules/module.api'
Vue.use(Api);


/**
 * good V
 */


Vue.config.devtools = true;

import store from './middlware/store'

import component from './component';
Object.keys(component).forEach(name => {
	Vue.component(name, component[name]);
});

import library from './middlware/library';
Object.keys(library).forEach(name => {
 	Vue.use(library[name].plugin, library[name].options || {});
});

import filters from './middlware/filters';
Object.keys(filters).forEach(name => {
	Vue.filter(name, filters[name]);
});

import index from './index.vue';

window.app = new Vue({
	el: '#app',
	filters: {},
	router,
	store,
	render: h => h(index)
});
