
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const autoprefixer = require('autoprefixer');
const CopyPlugin = require('copy-webpack-plugin');


const ENV = process.env.NODE_ENV;
const LOCAL = ENV === 'local';
const DEV = ENV === 'development';
const PROD = ENV === 'production';

const PATH = {
	src: path.resolve(__dirname, 'source_crm'),
	dist: path.resolve(__dirname, 'public_crm'),
};

const config = {
	entry: ["@babel/polyfill", PATH.src + '/app.js'],

	output: {
		path: PATH.dist,
		filename: 'js/script.js'
	},

	stats: {
		colors: true,
		errorDetails: true,
	},

	resolve: {
		extensions: ['.js', '.vue'],

		alias: {
			'@views': path.resolve(__dirname, 'source_crm/view'),
			'@store': path.resolve(__dirname, 'source_crm/middlware/store/index.js'),
			'@api': path.resolve(__dirname, 'source_crm/middlware/api/index.js'),
			'@util': path.resolve(__dirname, 'source_crm/middlware/util/index.js'),
			'@library': path.resolve(__dirname, 'source_crm/middlware/library'),
			'@component': path.resolve(__dirname, 'source_crm/component'),
		}
	},

	mode: ENV === 'production' ? 'production' : 'development',

	devServer: {
		contentBase: PATH.dist,
		compress: true,
		hot: true,
		inline: true,
		open: true,
		historyApiFallback: true
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			'window.$': 'jquery'
		}),
		new webpack.DefinePlugin({
			'isLocal': JSON.stringify(LOCAL),
			'isDev': JSON.stringify(DEV),
			'isProd': JSON.stringify(PROD),
			'process.env': {
				NODE_ENV: JSON.stringify(ENV)
			}
		}),
		new MiniCssExtractPlugin({
			filename: 'css/[name].css',
			chunkFilename: '[id].css'
		}),
		new HtmlWebpackPlugin({
			template: PATH.src + '/index.pug',
			inject: 'body'
		}),
		new VueLoaderPlugin(),
		new CopyPlugin([
			{ from: `${PATH.src}/data`, to: `${PATH.dist}` },
		]),
	],

	module:  {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					// postcss: [require('postcss-cssnext')()],
					// options: {
					//     extractCSS: true
					// },
					loaders: {
						// js: 'babel-loader'
					}
				}
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.pug$/,
				include: [path.resolve(__dirname, 'source_crm')],
				oneOf: [
					{
						resourceQuery: /^\?vue/,
						use: {
							loader: 'pug-plain-loader',
							options: {
								pretty: true,
								exports: false
							}
						}
					},
					// это применяется к импортам pug внутри JavaScript
					{
						use: [
							{
								loader: 'raw-loader',
								options: {
									pretty: true,
									exports: false
								}
							},
							{
								loader: 'pug-plain-loader',
								options: {
									pretty: true,
									exports: false
								}
							}
						]
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				// include: PATH.src + 'img/',
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							// outputPath: 'img/',
							useRelativePath: true
						}
					},

				]
			}, {
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					LOCAL ? 'style-loader' : {
							loader: MiniCssExtractPlugin.loader,
							options: {
								publicPath: '../'
							}
						},
					{
						loader: 'css-loader',
						options: {
							sourceMap: LOCAL,
							//minimize: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: [
								autoprefixer({
									// browsers:['ie >= 8', 'last 4 version']
								})
							],
							sourceMap: LOCAL,
							useRelativePath: true
						}
					},
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							sourceMap: LOCAL,
							data: `
								@import "source_crm/css/styles/var.scss";
								@import "source_crm/css/styles/mixins.scss";
							`
						},
					}
				]
			}, {
				test: /\.(eot|ttf|woff|woff2)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						// outputPath: 'fonts/',
						useRelativePath: true
					}
				}
			}, {
				test: /\.json/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'data/',
						useRelativePath: true
					}
				}
			}
		]
	},

	optimization: PROD ? {
		minimizer: [
			new UglifyJsPlugin({
				sourceMap: false,
				uglifyOptions: {
					//ecma: 5,
					compress: {
						inline: true,
						warnings: false,
						drop_console: false,
						unsafe: true
					},
				},
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorPluginOptions: {
					preset: ['default', { discardComments: { removeAll: true } }],
				},
			})
		],
	} : {}
};

module.exports = config;